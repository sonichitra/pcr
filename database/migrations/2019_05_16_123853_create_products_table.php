<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("product_name");
            $table->string("product_image",50)->nullable();

            $table->unsignedBigInteger("product_category")->nullable();
            $table->foreign('product_category')->references('id')->on('categories')->onDelete('cascade')->unsigned()->index();

            $table->unsignedBigInteger("user_id")->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->text("product_description");

            $table->string("product_price");
            $table->string("product_tag")->nullable();
            $table->string("product_tax")->nullable();
            $table->string("store_name")->nullable();
            $table->string("store_location")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
