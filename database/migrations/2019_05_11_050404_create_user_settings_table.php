<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_settings', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger("user_id");
            $table->foreign("user_id")->references("id")->on("users")->onDelete("cascade");

            $table->boolean("gift_notification")->comment("0=no,1=yes");
            $table->boolean("thankyou_message_notification")->comment("0=no,1=yes");

            $table->tinyInteger("whishlist_privacy")->comment("1=all contact,2=selected contact,3= all except selected,4 = GFTD Contact,5 = Selected GFTD Contact,6 = All GFTD contact except selected contact");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_settings');
    }
}
