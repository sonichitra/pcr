<?php

use Illuminate\Database\Seeder;

use App\Cms;

class CmsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [1,2];
    	foreach ($array as $key => $value) {
    		
            $Category = new Cms;
            $Category->type = $value;
	        $Category->save();
    		
    	}
    }
}
