<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReplyContactMail extends Mailable
{
    use Queueable, SerializesModels;

     public $var;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($var)
    {
       $this->var = $var;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('GFTD@gmail.com','GFTD')
                                            ->subject($this->var->subject)
                                            ->view('confirm.reply_mail');
        // return $this->view('view.name');
    }
}
