<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;

class BaseController extends Controller
{
    public function sendResponse($result, $message)
    {
        $response = [
            'data'    => $result,
            'message' => $message
        ];

        return response()->json($response);
    }

    public function sendError($error, $code = 404)
    {
        $response = [
            'error' => $error
        ];
        return response()->json($response,$code);
    }
}
