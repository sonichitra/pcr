<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\URL;
use Validator;
use DB;
use Mail;
use File;
use App\Category;

class CategoryController extends Controller
{
     public $successStatus = 200;
    /** 
     * News api 
     * 
     * @return \Illuminate\Http\Response 
     */ 

    public function index(Request $request){
    	try {
		 	$user = Auth::user(); 
		 	// return $user;	 	
		 	$category = Category::where('status',1)->get();
		 	$total_rows = Category::count();
            return response()->json(['success' => $category,'total_count'=>$total_rows,'msg'=>'success'], $this->successStatus); 
    	} catch (Exception $e) {
    		$msg = $e->getMessage();
	        return $this->sendError($msg);
    	}
    }
}
