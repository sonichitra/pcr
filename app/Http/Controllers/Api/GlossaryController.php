<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\User; 
use App\Favorites;
use App\Comments;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\URL;
use Validator;
use DB;
use Mail;
use File;
use App\Glossaries;

class GlossaryController extends BaseController
{
     public $successStatus = 200;
    /** 
     * News api 
     * 
     * @return \Illuminate\Http\Response 
     */ 

    public function glossaries(Request $request,$offset=0){
    	try {
		 	$user = Auth::user(); 
		 	// return $user;	 	
		 	$hasNextPage = true;
		 	$limit = 10;
      $offsetv = $offset * $limit;
		 	$glossary = DB::table('glossaries');
            $str = ''; 
            if(@$request->str){
                $str = strtolower(@$request->str);
                $glossary->Where( function ($query) use ($str) {
                        $query->where('title', 'LIKE','%'.$str.'%')->orWhere('description','LIKE','%'.$str.'%');
                });
            }
            $total_rows = $glossary->count();
            $glossary = $glossary->orderBy('id','desc')->offset($offsetv)->limit($limit)->get();		
            
           
			    if(count(@$glossary) < $limit){
    				$hasNextPage = false;
    			}
          return response()->json(['success' => $glossary,'total_count'=>$total_rows,'msg'=>'success','hasNextPage'=>$hasNextPage], $this->successStatus); 
    	} catch (Exception $e) {
    		$msg = $e->getMessage();
	        return $this->sendError($msg);
    	}
    }

}
