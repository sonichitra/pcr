<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\User; 
use App\Contactus;
use App\Cms;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\URL;
use Validator;
use DB;
use Mail;
use File;

class UsersController extends BaseController
{
     public $successStatus = 200;
    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
   
    public function login(Request $request){ 
    	try{
    		$validator = Validator::make($request->all(), [ 
	            'email' => 'required|email', 
	            'password' => 'required',  
	        ]);
	        if ($validator->fails()) { 
	           return $this->sendError($validator->messages()->first(), 402);           
	        }
	      if(Auth::attempt(['email' => $request->email, 'password' =>$request->password])){ 
	            $user = Auth::user(); 
	            if($user->status == 1)
	            {
		            $user_image = "";
			  		if(@$user->image){
				        $user_image = url('storage/user_images').'/'.$user->image;
				      }
		            $success['token'] =  $user->createToken('MyApp')->accessToken; 
		            $success['name'] =  $user->name;
		            $success['image'] =  @$user_image;
		            $success['email'] =  $user->email;
		            $success['mobile'] =  $user->mobile;
		            $success['status'] =  $user->status;
		            return response()->json(['success' => $success], $this->successStatus); 
	            }else{
	            	return $this->sendError('Your account has been blocked by admin.',404);
	            }
	        } 
	        else{ 
	        	return $this->sendError('Please enter valid email with password.',404);
	            //return response()->json(['error'=>'Unauthorised'], 401); 
	            
	        } 
	    } catch ( \Exception $e)  {
	        $msg = $e->getMessage();
	        return $this->sendError($msg);
	    }
    }
  
    /** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function details() 
    { 
    	try{
	        $user = Auth::user(); 
	        $user_image = "";
	  		if(@$user->image){
		        $user_image = url('storage/user_images').'/'.$user->image;
		      }
	        $resp = array(
	          "name" => @$user->name,
	          "image" => @$user_image,
	          "email" => @$user->email,
	          "mobile" => @$user->mobile,
	          "status" => @$user->status
	      	);
	        return response()->json(['success' => $resp, 'msg' => 'success' ], $this->successStatus); 
        } catch ( \Exception $e)  {
	        $msg = $e->getMessage();
	        return $this->sendError($msg);
	    }
    } 

    public function logout() {
    	try{
        	$accessToken = Auth::user()->token();
	        DB::table('oauth_refresh_tokens')
	            ->where('access_token_id', $accessToken->id)
	            ->update([
	                'revoked' => true
	            ]);

	        $accessToken->revoke();
	       return response()->json(['success' => (object)[], 'msg' => 'Loggedout successfully' ], $this->successStatus); 
        } catch ( \Exception $e)  {
	        $msg = $e->getMessage();
	        return $this->sendError($msg);
	    }
    }

  	public function changeProfile(Request $request){
  		
	  	$validation = Validator::make($request->all(),[
	  		'name'           => 'required|max:191',
	  		'mobile'         => 'required',
	  	]);
	  	if($validation->fails()){
	  		return $this->sendError($validation->messages()->first());
	  	}
	  	$user = Auth::user();
		$file = $request->file('user_image');
	  	if(@$file)
	  	{
	  		$old_image = @$user->image;
	  		//Move Uploaded File
            $filetype = $file->getClientOriginalExtension();
            $destinationPath = 'storage/user_images';
            $new_file_name = $user->id.time().'.'.$filetype;
            
            $file->move($destinationPath,$new_file_name);
			$fieldarray =  array(
		  		'name' => @$request->name,
		  		'mobile' => @$request->mobile,
		  		'image'=> @$new_file_name,
		  	);
	  		if( User::where("id",$user->id)->update($fieldarray)){
	  			if(@$old_image){
	  				$doc_path = @$destinationPath.'/'.@$old_image;
	                File::delete(@$doc_path);
		  		}
	  		}
	  		
	  	}else{
	  		$fieldarray =  array(
		  		'name' => $request->name,
		  		'mobile' => $request->mobile,
		  	);
		  	User::where("id",$user->id)->update($fieldarray);
	  	}
	  	if(@$request->password){
	      $password = bcrypt($request->password);
	      User::where("id",$user->id)->update(['password'=>$password]);
	    }
	  	
	  	$userresp = User::where("id",$user->id)->first();
	  	$resp = array();
	  	if(!empty($userresp)){
	  		$user_image = "";
	  		if(@$userresp->image){
		        $user_image = url('storage/user_images').'/'.$userresp->image;
		      }
  		 	$resp = array(
	          "name" => $userresp->name,
	          "image" => $user_image,
	          "email" => $userresp->email,
	          "mobile" => $userresp->mobile,
	          "status" => $userresp->status
	      	);

	  	}
	  	return response()->json(['success' => $resp, 'msg' => 'success' ], $this->successStatus); 
	 
	}

  	public function contact_us(Request $request){

	  	$validation = Validator::make($request->all(),[
	  		'name' => 'required|max:191',
	  		'email' => 'required|email|max:191',
	  		'mobile' => 'required|max:191',
	  	//	'subject' => 'required|max:191',
	  		'message' => 'required|max:191',
	  	]); 
	  	if($validation->fails()){
	  		return $this->sendError($validation->messages()->first());
	  	}

	  	$Contactus = new Contactus;
	  	$Contactus->name = $request->name;
	  	$Contactus->email = $request->email;
	  	$Contactus->mobile = $request->mobile;
	  	$Contactus->subject = 'Enquiry by '.ucfirst(@$request->name);
	  	$Contactus->message = $request->message;
	  	$Contactus->save();
	  	return response()->json(['success' => (object)[], 'msg' => 'Enquiry has been submitted successfully.' ], $this->successStatus); 
  	}

  	public function privacyPolicy()
    {
       $where = array(
        "type" => 2
       );
        $info =  Cms::where($where)->first();
        $resp = [];
        return $info['content'];
    }

  	public function termCondition()
  	{
	    $where = array(
	        "type" => 1
	    );
      	$info =  Cms::where($where)->first();
        return $info['content'];
	}
}
