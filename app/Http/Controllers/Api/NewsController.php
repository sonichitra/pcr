<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\User; 
use App\Favorites;
use App\Comments;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\URL;
use Validator;
use DB;
use Mail;
use File;
use App\News;

class NewsController extends BaseController
{
     public $successStatus = 200;
    /** 
     * News api 
     * 
     * @return \Illuminate\Http\Response 
     */ 

    public function news(Request $request,$offset=0){
    	try {
		 	$user = Auth::user(); 
		 	// return $user;	 	
		 	$hasNextPage = true;
		 	$limit = 10;
      $offsetv = $offset * $limit;
		 	$news = DB::table('news');
            $str = ''; $city = '';
            if(@$request->city){
                $city = strtolower(@$request->city);
                $news->where('city','LIKE','%'.$city.'%');
            }
            if(@$request->str){
                $str = strtolower(@$request->str);
                $news->Where( function ($query) use ($str) {
                        $query->where('title', 'LIKE','%'.$str.'%')->orWhere('description','LIKE','%'.$str.'%')->orWhere('city','LIKE','%'.$str.'%');
                });
            }
            $total_rows = $news->count();
            $news = $news->orderBy('id','desc')->offset($offsetv)->limit($limit)->get();		
            
            foreach ($news as $key => $v) {
                 $isfav = Favorites::where("user_id", @$user->id) -> where("news_id", @$v->id) -> first();
                  if (@($isfav)) {
                    $v->is_like = true;
                  } else {
                    $v->is_like = false;
                  }
                  $v->commentsCount = Comments::where(["news_id" => @$v->id,"parent"=>0])->count();
            }
			    if(count(@$news) < $limit){
    				$hasNextPage = false;
    			}
          return response()->json(['success' => $news,'total_count'=>$total_rows,'msg'=>'success','hasNextPage'=>$hasNextPage], $this->successStatus); 
    	} catch (Exception $e) {
    		$msg = $e->getMessage();
	        return $this->sendError($msg);
    	}
    }

    public function details(Request $request,$id){
    	try {
  		 	$user = Auth::user(); 		 	
  		 	$news = DB::table('news')->find($id);	
        $glossary = DB::table('glossaries')->where('status',1)->pluck('title')->toArray();
        /* code for search and replace */
        $string = @$news->description;
        $match = $this->contains($glossary, $string);
        $htm = '';
        $htm = 
        $news->description_htm = $match;
        /* end of search and replace code */
  		 	$comments = DB::table('comments')->where(['news_id'=> $id,"parent"=>0])->count();
        $isfav = Favorites::where("user_id", @$user->id) -> where("news_id", @$id) -> first();
          if (@($isfav)) {
            $is_like = true;
          } else {
            $is_like = false;
          }
			  $response = array('news'=>$news,'commentsCount'=>$comments, 'is_like' => $is_like);
        return response()->json(['success' => $response,'msg'=>'success'], $this->successStatus); 
    	} catch (Exception $e) {
    		$msg = $e->getMessage();
	      return $this->sendError($msg);
    	}
    }
   
    public function contains($needles, $haystack) {
      $needles = array_map('strtolower', $needles);
      $haystack = strip_tags($haystack);
      foreach ($needles as $match) {
          $haystack = str_replace($match, '<a href="javascript:void(0)" onclick="showGlossary('.$match.')"><b>'.$match.'</b></a>', $haystack);
      }
      return $haystack;
    }
   
}
