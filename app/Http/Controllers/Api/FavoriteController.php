<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\User; 
use App\Contactus;
use App\Cms;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\URL;
use Validator;
use DB;
use Mail;
use File;
use App\News;

class FavoriteController extends BaseController
{
     public $successStatus = 200;
    /** 
     * News api 
     * 
     * @return \Illuminate\Http\Response 
     */ 

    public function add(Request $request){
    	try {
            $user = Auth::user();
            $validation = Validator::make($request->all(),[
              'news_id'  => 'required',
              'folder_id'  => 'required',
            ]);

            if($validation->fails()){
              return $this->sendError($validation->messages()->first());
            }

            $insert = array(                
                'user_id'       => @$user['id'],
                'news_id'       => @$request->news_id, 
                'folder_id'     => @$request->folder_id, 
                'created_at'    => date('Y-m-d h:i:s'),                
            );
            DB::table('favorites')->insert($insert);            
            $lastId = DB::getPdo()->lastInsertId();
            $response = array('id'=>$lastId);
            return response()->json(['success' => $response,'msg'=>'success'], $this->successStatus); 
        } catch (Exception $e) {
            $msg = $e->getMessage();
            return $this->sendError($msg);
        }
    }

    public function createFolder(Request $request){
        try {
            $user = Auth::user();
            $validation = Validator::make($request->all(),[
              'folder_name'  => 'required',
            ]);

            if($validation->fails()){
              return $this->sendError($validation->messages()->first());
            }
            $isexist = DB::table('favorite_folders')->where(['user_id'=> @$user['id'],'name' =>@$request->folder_name])->first();
            if(@$isexist->name){
                return $this->sendError('Folder Name is already exist.');
            }
            $folder_image = "public/assets/img/no-img.png";
            $file = $request->file('folder_image');
            if(@$file){
                //Move Uploaded File
                $filetype = $file->getClientOriginalExtension();
                $destinationPath = 'storage/fav_folder_images';
                $foldername = trim(str_replace(' ', '', strtolower($request->folder_name)));
                $new_file_name = 'fld_'.$foldername.time().'.'.$filetype;
                $folder_image = $destinationPath.'/'.$new_file_name;
                $file->move($destinationPath,$new_file_name);
            }
            $insert = array(                
                'user_id'       => @$user['id'],
                'name'          => @$request->folder_name, 
                'image'         => @$folder_image,
                'created_at'    => date('Y-m-d h:i:s'),                 
            );
            DB::table('favorite_folders')->insert($insert);            
            $folderId = DB::getPdo()->lastInsertId();
            $response = array('id'=>$folderId,'name'=>$request->folder_name,'image'=>url(@$folder_image));
            return response()->json(['success' => $response,'msg'=>'success'], $this->successStatus); 
        } catch (Exception $e) {
            $msg = $e->getMessage();
            return $this->sendError($msg);
        }
        
    }

    public function getFolders(){
        try {                                          
            $user = Auth::user();      
            $folders = DB::table('favorite_folders')                            
                            ->where('user_id',$user['id'])                            
                            ->orderBy('id','desc')                            
                            ->get();  
            $response = array();  
            if(count(@$folders) > 0){
                foreach ($folders as $key => $value) {
                  $value->image = url(@$value->image);
                  $response[] = $value;
                }  
            } 
                                       
            return response()->json(['success' => $response,'msg'=>'success'], $this->successStatus); 
        } catch (Exception $e) {
            $msg = $e->getMessage();
            return $this->sendError($msg);
        }   
    }

    public function get(Request $request,$folderId,$offset=0){
        try {
            $user = Auth::user(); 
            // return $user;        
            $hasNextPage = false;
            $limit = 20;
            $news = DB::table('favorites')
                        ->join('news', 'news.id', '=', 'favorites.news_id')                    
                        ->select('news.*')
                        ->where('favorites.user_id','=',$user['id'])
                        ->where('favorites.folder_id','=',$folderId)
                        ->orderBy('news.id','desc')
                        ->offset($offset)
                        ->limit($limit)
                        ->get();    

            $total_rows = DB::table('favorites')
                            ->join('news', 'news.id', '=', 'favorites.news_id')
                            ->where('favorites.user_id', '=', $user['id'])
                            ->where('favorites.folder_id', '=', $folderId)
                            ->select('news.*')                            
                            ->count();
            if($total_rows > ($offset+$limit) ){
                $hasNextPage = true;
            }
            return response()->json(['success' => $news,'msg'=>'success','hasNextPage'=>$hasNextPage], $this->successStatus); 
        } catch (Exception $e) {
            $msg = $e->getMessage();
            return $this->sendError($msg);
        }
    }

    public function delete(Request $request,$newsId){
        try {
            $user = Auth::user();             
            DB::table('favorites')
            ->where('user_id', '=', $user['id'])
            ->where('news_id', '=', $newsId)
            ->delete();            
            return response()->json(['success' =>true, 'msg'=>'success'], $this->successStatus); 
        } catch (Exception $e) {
            $msg = $e->getMessage();
            return $this->sendError($msg);
        }
    }
   
}
