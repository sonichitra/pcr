<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\User; 
use App\Contactus;
use App\Cms;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\URL;
use Validator;
use DB;
use Mail;
use File;
use Carbon\Carbon;
use App\News;

class CommentsController extends BaseController
{
     public $successStatus = 200;
    /** 
     * Comments api 
     * 
     * @return \Illuminate\Http\Response 
     */
 
    public function comments(Request $request,$news_id,$offset=0){
        try {
            $user = Auth::user(); 
            // return $user;        
            $hasNextPage = true;
            $limit = 10;
            $offsetv = ($offset * $limit);
            $comments = DB::table('comments')
                            ->join('users as u', 'u.id', '=', 'comments.user_id')
                            ->leftJoin('comments_ratings as cr', 'cr.comment_id', '=', 'comments.id')
                            ->select(['comments.*','u.image','u.name','u.email','u.mobile','cr.is_like'])
                            ->where('comments.parent', '=', 0)
                            ->where('comments.news_id', '=', $news_id)
                            ->groupBy('comments.id')
                            ->orderBy('comments.id','desc')
                            ->offset($offsetv)
                            ->limit($limit)
                            ->get();
            $replies = array();
            $response = array();
            if(@$comments){
                $i = 0;
                foreach ($comments as $key => $value) {

                    $reply = DB::table('comments')
                        ->join('users as u', 'u.id', '=', 'comments.user_id')
                        ->leftJoin('comments_ratings as cr', 'cr.comment_id', '=', 'comments.id')
                        ->select(['comments.*','u.image','u.name','u.email','u.mobile','cr.is_like'])
                        ->where('parent', '=',$value->id)
                        ->where('news_id', '=',$news_id)
                        ->orderBy('id','asc')
                        ->get();
                    if(@$reply){
                        foreach ($reply as $rk => $rv) {
                            $reply_comments_like = DB::table('comments_ratings')
                                                    ->where(['comment_id'=>$rv->id,'is_like'=>1])
                                                    ->count();
                            $rv->like_count = $reply_comments_like;
                            if(@$rv->image){
                                $rv->image = url('storage/user_images').'/'.$rv->image;
                              }else{
                                $rv->image = '';
                              }
                             $replies[] = $rv;
                        }
                       
                    }
                    if($value->parent == 0){
                        $value->reply_count = count(@$replies);
                        $comments_like = DB::table('comments_ratings')
                                        ->where(['comment_id'=>$value->id,'is_like'=>1])
                                        ->count();
                        $value->like_count = $comments_like;
                          if(@$value->image){
                            $value->image = url('storage/user_images').'/'.$value->image;
                          }else{
                            $value->image = '';
                          }
                        $response[$i] = (array)$value;
                        $response[$i]['replies'] = $replies;
                        $replies = array();
                        $i++;
                    }
                }
            }
            $total_rows = DB::table('comments')
                            ->where('news_id', '=',$news_id)
                            ->where('parent', '=',0)
                            ->count();
            if(count(@$comments) < $limit){
                $hasNextPage = false;
            }

            return response()->json(['success' => $response,'total_count'=>$total_rows,'hasNextPage'=>$hasNextPage,'msg'=>'success'], $this->successStatus); 
        } catch (Exception $e) {
            $msg = $e->getMessage();
            return $this->sendError($msg);
        }
    }
    public function replies(Request $request,$parent_id,$offset=0){
        try {                        
            $hasNextPage = false;
            $limit = 20;
            $replies = DB::table('comments')
                            ->where('parent_id', '=', $parent_id)
                            ->orderBy('id','desc')
                            ->offset($offset)
                            ->limit($limit)
                            ->get();           
            $total_rows = DB::table('comments')->where('parent_id', '=', $parent_id)->count();
            if($total_rows > ($offset+$limit) ){
                $hasNextPage = true;
            }
            return response()->json(['success' => $replies,'msg'=>'success','hasNextPage'=>$hasNextPage], $this->successStatus); 
        } catch (Exception $e) {
            $msg = $e->getMessage();
            return $this->sendError($msg);
        }
    }
   
    public function add_comments(Request $request){
        $mytime = Carbon::now();
        try {
            $validator = Validator::make($request->all(), [ 
                'news_id' => 'required', 
                'comment' => 'required', 
                'parent'  => 'required'
            ]);
            if ($validator->fails()) { 
               return $this->sendError($validator->messages()->first(), 402);           
            }
            $user = Auth::user(); 

            $insert = array(                
                'user_id'       => @$user['id'],
                'news_id'       => @$request->news_id, 
                'comment'       => @$request->comment, 
                'parent'        => @$request->parent,
                // 'created_at'    => date('Y-m-d h:i:s'),
                'created_at'    => $mytime->toDateTimeString(),
                                
            );
            DB::table('comments')->insert($insert);            
            $lastId = DB::getPdo()->lastInsertId();
            $response = array('id'=>$lastId);
            return response()->json(['success' => $response,'msg'=>'success'], $this->successStatus); 
           
        } catch (Exception $e) {
            $msg = $e->getMessage();
            return $this->sendError($msg);
        }
    }

    public function like_dislike(Request $request){
        try {
            $validator = Validator::make($request->all(), [ 
                'comment_id' => 'required', 
                'is_like' => 'required', 
            ]);
            if ($validator->fails()) { 
               return $this->sendError($validator->messages()->first(), 402);           
            }
            $user = Auth::user(); 

            $fieldArray = array(                
                'user_id'       => @$user['id'],
                'is_like'       => @$request->is_like, 
                'comment_id'    => @$request->comment_id, 
                'updated_at'    => date('Y-m-d H:i:s'),
                                
            );
            $where = array(
                'user_id'       => @$user['id'],
                'comment_id'    => @$request->comment_id, 
            );
            DB::table('comments_ratings')->updateOrInsert($where,$fieldArray);
            $like_count = DB::table('comments_ratings')
                        ->where('comment_id', '=', $request->comment_id)
                        ->where('is_like', '=', 1)
                        ->count();
            $reply_count = DB::table('comments')
                        ->where('parent', '=', $request->comment_id)
                        ->count();
            $response = DB::table('comments')
                        ->join('users as u', 'u.id', '=', 'comments.user_id')
                        ->leftJoin('comments_ratings as cr', 'cr.comment_id', '=', 'comments.id')
                        ->select(['comments.*','u.image','u.name','u.email','u.mobile','cr.is_like'])
                        ->where('comments.id', '=', $request->comment_id)
                        ->where('cr.user_id', '=', @$user['id'])
                        ->first();
            if(@$response->image){
                $response->image = url('storage/user_images').'/'.$response->image;
              }else{
                $response->image = '';
              }           
            $response->like_count = $like_count;
            $response->reply_count = $reply_count;
            return response()->json(['success' => $response,'msg'=>'success'], $this->successStatus); 
           
        } catch (Exception $e) {
            $msg = $e->getMessage();
            return $this->sendError($msg);
        }
    }
}
