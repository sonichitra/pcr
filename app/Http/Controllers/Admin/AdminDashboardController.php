<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Cms;
use Validator;
use DB;

class AdminDashboardController extends Controller
{
    public function index(){
        $data['active'] = 'Dashboard';
        // $data['allusers'] = User::count('id');
        // $data['activeusers'] = User::where('status',1)->count('id');
        // $data['deactiveusers'] = User::where(function($q){
        // 	$q->where('status',2)->orwhere('status',3);
        // })->count('id');
       return view("admin.home",$data);
    	//return view("confirm.dashboard",$data);
    }

    public function term_condition(){
        $record['active'] = 'Terms_Condition';
    	$record['terms'] = Cms::where("type",1)->first();
    	//return view("confirm.termscondition",$record);
        return view("admin.termscondition",$record);
    }

    public function term_condition_update(Request $request){
    	$Validator = Validator::make($request->all(),[
    		"detail" => "required"
    	]);

    	if($Validator->fails()){
    		return redirect()->back()->withErrors($Validator);
    	}

    	
        $where = array(
            'type'  =>  1
        );
        $fieldArray = array(
            'type'  =>  1,
            'content' => @$request->detail,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        );
        DB::table('cms')->updateOrInsert($where,$fieldArray);
    	return redirect()->back()->with("message","Terms and condition updated successfully.");
    }

    public function policy(){
        $record['active'] = 'Policy';
        $record['terms'] = Cms::where("type",2)->first();
        //return view("confirm.policy",$record);
         return view("admin.policy",$record);
    }

    public function policy_update(Request $request){
        $Validator = Validator::make($request->all(),[
            "detail" => "required"
        ]);

        if($Validator->fails()){
            return redirect()->back()->withErrors($Validator);
        }

        $where = array(
            'type'  =>  2
        );
        $fieldArray = array(
            'type'  =>  2,
            'content' => @$request->detail,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        );
        DB::table('cms')->updateOrInsert($where,$fieldArray);
        return redirect()->back()->with("message","Privacy Policy updated successfully.");
    }
}
