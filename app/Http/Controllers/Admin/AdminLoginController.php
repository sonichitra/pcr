<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Admin;
use Validator;

class AdminLoginController extends Controller
{
    public function login(){
    	return view("login");
    }

    public function adminlogin(Request $request){
        $validation = Validator::make($request->all(),[
        	'email' => 'required',
        	'password' => 'required',
        ]);

        if($validation->fails()){
        	return redirect()->back()->withErrors($validation)->withInput();
        }

        $admin = Admin::where(["email" => $request->email,"password" => $request->password])->first();
        if(@$admin){
          
        }else{
        	return redirect()->back()->with("error_message","Please Enter Valid Email Or Password.");
        }
    }
}
