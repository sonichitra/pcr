<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contactus;
use Validator;
use App\Mail\ReplyContactMail;
use Mail;

class ContactUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['active'] = 'Contactus';
        $data['list'] = Contactus::orderby("id","desc")->get();
        //return view("confirm.contactus",$data);
         return view("admin.contactus",$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Validator = Validator::make($request->all(),[
            "id" => "required",
            "message_reply" => "required"
        ]);

        if($Validator->fails()){
            return redirect()->back()->withErrors($Validator);
        }

        $Email = Contactus::where("id",$request->id)->select("email")->first();

            $objDemo1 = new \stdClass();
            $objDemo1->subject = "GFTD Replay Mail";
            $objDemo1->body = $request->message_reply;



        $Contactus = Contactus::where("id",$request->id)->update(["message_reply" => $request->message_reply,"status" => 2]);
        if(@$Contactus){
            @Mail::to($Email->email)->send(new ReplyContactMail($objDemo1));
            return redirect()->back()->with("message","Message Replied Successfully.");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $request->all();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
