<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;

class AdminUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['active'] = "Users";
        $data['users'] = User::where("status",1)->get();
        $users = User::select('*');
        $str = '';
        $searchby = @$request->searchby;
        $sortby = @$request->sortby;
        if(@$request->str){
            $str = strtolower(@$request->str);
            $users->Where( function ($query) use ($str) {
                    $query->where('name', 'LIKE','%'.$str.'%')->orWhere('email','LIKE','%'.$str.'%')->orWhere('mobile','LIKE','%'.$str.'%');
            });
        }
        if(!empty($searchby)){
            $users->where('status',$searchby);
        }
        if(!empty($sortby)){
            $expsort = explode('-', $sortby);
            $users->orderBy(@$expsort[0],@$expsort[1]);
        }else{
            $users->orderBy('id','desc');
        }
        $users = $users->paginate(20); 
        $users->appends (array ('str' => @$str,'searchby' => @$searchby,'sortby' => @$sortby));     
        $data['users'] = $users;


       // $data['users'] = User::where("status",1)->orderby("id","desc")->get();
        return view("admin.users",$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function userdeactive($uid){
       $aid = Auth::guard('admin')->user()->id;
       $dd = User::where("id",$uid)->update(["status"=>3,"updated_by" => $aid]);
       if($dd){
        return redirect()->back()->with("message","User Deactivated successfully.");
       }
    }

    public function useractive($uid){
       $aid = Auth::guard('admin')->user()->id;
       $dd = User::where("id",$uid)->update(["status"=>1,"updated_by" => $aid]);
       if($dd){
        return redirect()->back()->with("message","User Activated successfully.");
       }
    }
}
