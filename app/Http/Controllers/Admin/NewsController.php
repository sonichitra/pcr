<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\News;
use DB;

class NewsController extends Controller
{
    public function index(Request $request)
    {
        $data['active'] = 'News';
        $data['sub_active'] = '';
        $data['sub_active1'] = '';
        $news = DB::table('news');
      	if(@$request->city){
            $city = strtolower(@$request->city);
            $news->where('city','LIKE','%'.$city.'%');
        }
        if(@$request->str){
            $str = strtolower(@$request->str);
            $news->Where( function ($query) use ($str) {
                $query->where('title', 'LIKE','%'.$str.'%')->orWhere('description','LIKE','%'.$str.'%')->orWhere('city','LIKE','%'.$str.'%');
            });
        }
        $searchby = @$request->searchby;
        $sortby = @$request->sortby;
        if(!empty($searchby)){
            $news->where('status',$searchby);
        }
        if(!empty($sortby)){
            $expsort = explode('-', $sortby);
            $news->orderBy(@$expsort[0],@$expsort[1]);
        }else{
            $news->orderBy('id','desc');
        }
        $news = $news->paginate(30);	
        $news->appends (array ('str' => @$str,'searchby' => @$searchby,'sortby' => @$sortby));  
        $data['news'] = $news;
        return view("admin.news", $data);
    }

    public function detail($id)
    {
        $data['active'] = 'News';
        $data['sub_active'] = '';
        $data['sub_active1'] = '';
        $news = DB::table('news')->where('id',$id)->first();
        $data['comment_count'] = DB::table('comments')->where(['news_id'=> $id,"parent"=>0])->count();
        $data['fav_count'] = DB::table('favorites')->where("news_id", @$id)->count();
        $data['news'] = $news;
        
        $commentResp = array();
        $comments = DB::table('comments')
                    ->join('users as u', 'u.id', '=', 'comments.user_id')
                    ->leftJoin('comments_ratings as cr', 'cr.comment_id', '=', 'comments.id')
                    ->select(['comments.*','u.image','u.name','u.email','u.mobile','cr.is_like'])
                    ->where('comments.parent', '=', 0)
                    ->where('comments.news_id', '=', $id)
                    ->groupBy('comments.id')
                    ->orderBy('comments.id','desc')
                    ->get();
        if(!empty($comments)){
            $i = 0;
            foreach ($comments as $key => $value) {
                $reply = DB::table('comments')
                        ->join('users as u', 'u.id', '=', 'comments.user_id')
                        ->leftJoin('comments_ratings as cr', 'cr.comment_id', '=', 'comments.id')
                        ->select(['comments.*','u.image','u.name','u.email','u.mobile','cr.is_like'])
                        ->where('parent', '=',$value->id)
                        ->where('news_id', '=',$id)
                        ->orderBy('id','asc')
                        ->get();
                if(@$reply){
                        foreach ($reply as $rk => $rv) {
                            $reply_comments_like = DB::table('comments_ratings')
                                                    ->where(['comment_id'=>$rv->id,'is_like'=>1])
                                                    ->count();
                            $rv->like_count = $reply_comments_like;
                            if(@$rv->image){
                                $rv->image = url('storage/user_images').'/'.$rv->image;
                              }else{
                                $rv->image = '';
                              }
                            $rv->time_ago = $this->dateDiff(date('Y-m-d H:i:s'),@$rv->created_at,3);
                            $replies[] = $rv;
                        }
                       
                    }
                if($value->parent == 0){
                    $value->time_ago = $this->dateDiff(date('Y-m-d H:i:s'),@$value->created_at,3);
                    $commentResp[$i] = (array)$value;
                    $commentResp[$i]['replies'] = $replies;
                    $replies = array();
                    $i++;
                }
            }
        }
        $data['commentResp'] = $commentResp;
        return view("admin.news_detail", $data);
    }

    public function newsdeactive($id){
       $dd = News::where("id",$id)->update(["status"=>2,"updated_at" => date('Y-m-d H:i:s')]);
       if($dd){
        return redirect()->back()->with("message","News Deactivated successfully.");
       }
    }

    public function newsactive($id){
       $dd = News::where("id",$id)->update(["status"=>1,"updated_at" => date('Y-m-d H:i:s')]);
       if($dd){
        return redirect()->back()->with("message","News Activated successfully.");
       }
    }

    public function dateDiff($time1, $time2, $precision = 6) {
            // If not numeric then convert texts to unix timestamps
            if (!is_int($time1)) {
              $time1 = strtotime($time1);
            }
            if (!is_int($time2)) {
              $time2 = strtotime($time2);
            }

            // If time1 is bigger than time2
            // Then swap time1 and time2
            if ($time1 > $time2) {
              $ttime = $time1;
              $time1 = $time2;
              $time2 = $ttime;
            }

            // Set up intervals and diffs arrays
            $intervals = array('year','month','day','hour','minute','second');
            $diffs = array();

            // Loop thru all intervals
            foreach ($intervals as $interval) {
              // Create temp time from time1 and interval
              $ttime = strtotime('+1 ' . $interval, $time1);
              // Set initial values
              $add = 1;
              $looped = 0;
              // Loop until temp time is smaller than time2
              while ($time2 >= $ttime) {
                // Create new temp time from time1 and interval
                $add++;
                $ttime = strtotime("+" . $add . " " . $interval, $time1);
                $looped++;
              }
         
              $time1 = strtotime("+" . $looped . " " . $interval, $time1);
              $diffs[$interval] = $looped;
            }
            
            $count = 0;
            $times = array();
            // Loop thru all diffs
            foreach ($diffs as $interval => $value) {
              // Break if we have needed precission
              if ($count >= $precision) {
                break;
              }
              // Add value and interval 
              // if value is bigger than 0
              if ($value > 0) {
                // Add s if value is not 1
                if ($value != 1) {
                  $interval .= "s";
                }
                // Add value and interval to times array
                $times[] = $value . " " . $interval;
                $count++;
              }
            }

            // Return string with times
            return implode(", ", $times);

    }
}
