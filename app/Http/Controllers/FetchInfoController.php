<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;
use App\Category;
use DB;
class FetchInfoController extends Controller
{
	public function addNews(){
		/*$city = '';
	    if($no == 1){ //Newyork -NY times
	    	$city = 'Newyork';
			$url = "https://rss.nytimes.com/services/xml/rss/nyt/NYRegion.xml";
		}else if($no == 2){ //-Abc 7
			$city = 'Newyork';
			$url = "https://abc7ny.com/feed/";
		}else if($no == 3){ //San Francisco -Abc 7
			$city = 'San Francisco';
			$url = "https://abc7news.com/feed/";
		}else if($no == 4){ //-SF GATE
			$url = "https://www.sfgate.com/rss/";
		}else if($no == 5){ //Los Angeles-La Times
			$url = "https://www.latimes.com/feeds";
		}else if($no == 6){ //-Dailynews
			$city = "Los Angeles";
			$url = "https://www.dailynews.com/news/feed/";
		}else if($no == 7){ //-La Weekly
			$url = "https://www.laweekly.com/rss-feeds-page/";
		}*/
		/* $urlArray[] =  array(
				'cityname' => 'Newyork',
				'url'=> 'https://rss.nytimes.com/services/xml/rss/nyt/NYRegion.xml'
		);
		$urlArray[] =	array(
				'cityname' => 'Newyork',
				'url'=> 'https://abc7ny.com/feed/'
		);
	    $urlArray[] =	array(
				'cityname' => 'San Francisco',
				'url'=> 'https://abc7news.com/feed/'
		);
		$urlArray[] =	array(
				'cityname' => 'Los Angeles',
				'url'=> 'https://www.dailynews.com/news/feed/'
		);
		$urlArray[] =	array(
				'cityname' => 'Los Angeles',
				'url'=> 'https://www.latimes.com/world-nation/rss2.0.xml'
		);
		$urlArray[] =	array(
				'cityname' => 'Los Angeles',
				'url'=> 'https://www.latimes.com/travel/rss2.0.xml'
		);
		$urlArray[] =	array(
				'cityname' => 'Los Angeles',
				'url'=> 'https://www.latimes.com/science/rss2.0.xml'
		);
		$urlArray[] =	array(
				'cityname' => 'Los Angeles',
				'url'=> 'https://www.latimes.com/politics/rss2.0.xml'
		);
		$urlArray[] =	array(
				'cityname' => 'Los Angeles',
				'url'=> 'https://www.latimes.com/opinion/rss2.0.xml'
		);
		$urlArray[] =	array(
				'cityname' => 'Los Angeles',
				'url'=> 'https://www.latimes.com/obituaries/rss2.0.xml'
		);
		$urlArray[] =	array(
				'cityname' => 'Los Angeles',
				'url'=> 'https://www.latimes.com/environment/rss2.0.xml'
		);
		$urlArray[] =	array(
				'cityname' => 'Los Angeles',
				'url'=> 'https://www.latimes.com/california/orange-county/rss2.0.xml#nt=1col-7030col1'
		);*/ 
		$urlArray[] =	array(
				'cityname' => 'Los Angeles',
				'url'=> 'https://www.latimes.com/business/rss2.0.xml'
		);
		$urlArray[] =	array(
				'cityname' => 'Los Angeles',
				'url'=> 'https://www.latimes.com/business/technology/rss2.0.xml#nt=1col-7030col1'
		);
		$urlArray[] =	array(
				'cityname' => 'Los Angeles',
				'url'=> 'https://www.latimes.com/business/real-estate/rss2.0.xml'
		);
		$urlArray[] =	array(
				'cityname' => 'Los Angeles',
				'url'=> 'https://www.latimes.com/business/autos/rss2.0.xml'
		);
		
		foreach ($urlArray as $key => $uv) {
			// echo $uv['cityname']." => ".$value['uv'];
			// echo "<hr>";
			 $cityname = $uv['cityname'];
			 //echo (string)$uv['url']; exit();
			 $domOBJ = new \DOMDocument();
 			 
			 $domOBJ->load($uv['url']);//XML page URL
			 $channel =  $domOBJ->getElementsByTagName("channel");
			foreach( $channel as $da )
			{
			 $channel_title = $da->getElementsByTagName("title")->item(0)->nodeValue;
			 $channel_pic = $da->getElementsByTagName("image");
			 // foreach ($da->getElementsByTagName('image') as $node) {
				// $channel_pic = $node->getElement('link')->item(0)->nodeValue;
				// }
			}
			$content = $domOBJ->getElementsByTagName("item");

			 foreach( $content as $data )
			 {


			   $title = $data->getElementsByTagName("title")->item(0)->nodeValue;
			   $link = $data->getElementsByTagName("link")->item(0)->nodeValue;
			   $description = $data->getElementsByTagName("description")->item(0)->nodeValue;
			   $guid = $data->getElementsByTagName("guid")->item(0)->nodeValue;
			   $pubDate = $data->getElementsByTagName("pubDate")->item(0)->nodeValue;
			   $category = $data->getElementsByTagName("category");
			   $ctg = array(); $categoryid = array();
			  // $images = $data->getElementsByTagName('media');
			   $nlContent = $data->getElementsByTagNameNS("http://search.yahoo.com/mrss/", "content");
			    if( $nlContent->length > 0 )
			    {
		    	 	$item_pic = $nlContent->item(0)->getAttribute('url');
		    	 	
		    	}
		    	else if(@$channel_pic and empty($item_pic)){
		    		$item_pic = "";
		    	}else{
		    		$item_pic = 'http://psych.autolends.com/public/assets/img/no-img.png';
		    	}
			   foreach ($category as $key => $value) {
			   	$ctgname = trim(preg_replace('/\s\s+/', ' ', $data->getElementsByTagName("category")->item($key)->nodeValue));
			   	$ctgname =  trim(preg_replace ('/[^a-zA-Z0-9\']/', ' ', $ctgname));
			   	 $ctg[] =  $ctgname;
			   	 $categoryArray = array(
			   	 	'category_name' => $ctgname,
			   	 	'created_at' => date('Y-m-d h:i:s'),
			   	 	'updated_at' => date('Y-m-d h:i:s')
			   	 );
			      $where = array(
			      		'category_name' => $ctgname
			      );
			   	 DB::table('categories')->updateOrInsert($where,$categoryArray);
                  $lastInsertId = DB::table('categories')->select('id')->where($where)->first();
                  $categoryid[] = @$lastInsertId->id;
			   }
			  
			    $ctg = implode(', ', $ctg);
	   			$fieldArray= array(
	   				//'chanel' => @$channel_title,
		    		'city' => @$cityname,
		    		'title' => trim(preg_replace('/\s\s+/', ' ', @$title)),
		    		'image' => @$item_pic,
		    		'description' => trim(preg_replace('/\s\s+/', ' ', @$description)),
		    		'link' => trim(preg_replace('/\s\s+/', ' ', @$link)),
		    		'guid' => trim(preg_replace('/\s\s+/', ' ', @$guid)),
		    		'publish_date' => strtotime(@$pubDate),
		    		'category' => $ctg,
		    		'category_id' => json_encode(@$categoryid),
		    		'created_at' => date('Y-m-d h:i:s'),
			   	 	'updated_at' => date('Y-m-d h:i:s')
		    	);
		    	$nwhere = array(
		    			'guid' => trim(preg_replace('/\s\s+/', ' ', @$guid))
		    	);
		    	DB::table('news')->updateOrInsert($nwhere,$fieldArray);
		    	
		    	$lastId = DB::table('news')->select('id')->where($nwhere)->first();
		    	echo  'success Id : '.$lastId->id."<hr>";
			 }
		}
		return $fieldArray;
		 // if(News::insert($fieldArray)){
		 // 	echo 'Inserted Successfully';
		 // }else{
		 // 	echo 'error';
		 // }
		 return $fieldArray; 
		 //dd($fieldArray); 
		  exit();
		
	}
	public function getNews($no){
	
		
		 // $invalidurl = false;
		 // if(@simplexml_load_file($url)){
		 //  $feeds = simplexml_load_file($url);
		 // }else{
		 //  $invalidurl = true;
		 //  echo "<h2>Invalid RSS feed URL.</h2>";
		 // }


		 // $i=0;
		 // if(!empty($feeds)){

		 // 		  	$site = $feeds->channel->title;
			//   	$sitelink = $feeds->channel->link;
		  // echo "<h1>".$site."</h1>";
		/*  foreach ($feeds->channel->item as $item) {

		   $title = $item->title;
		   $link = $item->link;
		   $description = $item->description;
		   $postDate = $item->pubDate;
		   $pubDate = date('D, d M Y',strtotime($postDate));
		   $category = $item->category;

		  ?>
		  <h1><?php echo $category; ?></h1>
		   <div class="post">
		     <div class="post-head">
		       <h2><a class="feed_title" href="<?php echo $link; ?>"><?php echo $title; ?></a></h2>
		       <span><?php echo $pubDate; ?></span>
		     </div>
		     <div class="post-content">
		       <?php echo implode(' ', array_slice(explode(' ', $description), 0, 20)) . "..."; ?> <a href="<?php echo $link; ?>">Read more</a>
		     </div>
		   </div>

		   <?php
		    $i++;
		   } */

		 //    $channel = $feeds->channel;
			// echo "<h1><a href=\"{$channel->link}\">{$channel->title}</a></h1>\n";
			// echo "{$channel->description}\n";
			// echo "<dl>\n";
			// foreach ($channel->item as $item) {
			//     echo "<dt><a href=\"{$item->link}\">{$item->title}</a></dt>\n"
			//     . "<dd style=\"margin-bottom: 30px;\"><div style=\"font-size: small;\">{$item->pubDate}</div>\n"
			//     . "<div>{$item->description}</div>\n"
			//     . "Categories: <strong>".implode('</strong>, <strong>', (array) $item->category) . "</strong>\n</dd>";
			// }
			// echo "</dl>\n";
		 // }else{
		 //   if(!$invalidurl){
		 //     echo "<h2>No item found</h2>";
		 //   }
		 // }
		
		 // exit();
	
		if($no == 1){ //Newyork -NY times
			$url = "https://rss.nytimes.com/services/xml/rss/nyt/NYRegion.xml";
		}else if($no == 2){ //-Abc 7
			$url = "https://abc7ny.com/feed/";
		}else if($no == 3){ //San Francisco -Abc 7
			$url = "https://abc7news.com/feed/";
		}else if($no == 4){ //-SF GATE
			$url = "https://www.sfgate.com/rss/";
		}else if($no == 5){ //Los Angeles-La Times
			$url = "https://www.latimes.com/feeds";
		}else if($no == 6){ //-Dailynews
			$url = "https://www.dailynews.com/news/feed/";
		}else if($no == 7){ //-La Weekly
			$url = "https://www.laweekly.com/rss-feeds-page/";
		}
		else if($no == 8){ //-La Weekly
			$url = "https://www.latimes.com/business/rss2.0.xml";
		}
		else if($no == 9){ //-La Weekly
			$url = "https://www.latimes.com/business/technology/rss2.0.xml#nt=1col-7030col1";
		}
		else if($no == 10){ //-La Weekly
			$url = "https://www.latimes.com/business/real-estate/rss2.0.xml";
		}
		else if($no == 11){ //-La Weekly
			$url = "https://www.latimes.com/business/autos/rss2.0.xml";
		}
		$imageUrls = array();
		 if(@simplexml_load_file($url)){
		  $feeds = simplexml_load_file($url);
		  return $feeds;
		  		foreach ($feeds->channel->item as $item) {
		  			$ns_media = $item->children('http://search.yahoo.com/mrss/');

    				 $imageurl = (array)@$ns_media->content->attributes()->url;
    				 
    				 array_push($imageUrls, $imageurl);
    				 // foreach ($imageurl as $key => $value) {
    				 // 	echo $value;
    				 // }
		  			//array_push($imageUrls, $imageurl);
					 /*  $title = $item->title;
					   $link = $item->link;
					   $description = $item->description;
					   $postDate = $item->pubDate;
					   $pubDate = date('D, d M Y',strtotime($postDate));
					   $category = $item->category;

					  ?>
					  <h1><?php echo $category; ?></h1>
					   <div class="post">
					   	<img alt="iamge" src="<?php (string)$item->enclosure['url'][0] ?>">
					     <div class="post-head">
					       <h2><a class="feed_title" href="<?php echo $link; ?>"><?php echo $title; ?></a></h2>
					       <span><?php echo $pubDate; ?></span>
					     </div>
					     <div class="post-content">
					       <?php echo implode(' ', array_slice(explode(' ', $description), 0, 20)) . "..."; ?> <a href="<?php echo $link; ?>">Read more</a>
					     </div>
					   </div>

					   <?php */

		   }
		   dd($imageUrls);
		 }else{
		  $invalidurl = true;
		  echo "<h2>Invalid RSS feed URL.</h2>";
		 }
		 exit();
			$curl = curl_init();

			curl_setopt_array($curl, Array(
			    CURLOPT_URL            => $url,
			    CURLOPT_RETURNTRANSFER => TRUE,
			    CURLOPT_ENCODING       => 'UTF-8'
			));

			$data = curl_exec($curl);
			curl_close($curl);

			$xml = simplexml_load_string($data);
			$json = json_encode($xml);
			$array = json_decode($json,TRUE);
			return  $array;
			// $feeds = (array) $array;
			// //return $feeds;
			// if(!empty($feeds)){
			// 	   	$channel = (array) $feeds->channel;
			// 	    foreach ($channel->item as $item) {
			// 	    	$category = (array) $item->category;
			// 	    	 $ctg = '';
			// 	    	// foreach ($category as $key => $value) {
			// 	    	// 	$ctg .= $value.", ";
			// 	    	// }
			// 	    	$fieldArray[] = array(
			// 	    		'city' => @$city,
			// 	    		'title' => @$item->title[0],
			// 	    		'description' => @$item->description[0],
			// 	    		'link' => @$item->link[0],
			// 	    		'guid' => @$item->guid[0],
			// 	    		'publish_date' => @$item->pubDate[0],
			// 	    		'category' => $ctg,
			// 	    	);
			// 		}
			// 		return $fieldArray; 
			// }else{
			//   $invalidurl = true;
			//   echo "<h2>Invalid RSS feed URL.</h2>";
			// }
			// return $array;
	}

	function crawl_page()
	{
		$url = 'https://opentextbc.ca/socialpsychology/back-matter/glossary-2/'; 
		$depth = 5;
	    static $seen = array();
	    if (isset($seen[$url]) || $depth === 0) {
	        return;
	    }

	    $seen[$url] = true;

	   $domOBJ = new \DOMDocument();
 			 
		 $domOBJ->load("https://opentextbc.ca/socialpsychology/back-matter/glossary-2/");//HTML page URL
		 // $channel =  $domOBJ->getElementsByTagName("channel");
	  //   @$dom->loadHTMLFile($url);

	    $anchors = $domOBJ->getElementsByTagName('a');
	    foreach ($anchors as $element) {
	        $href = $element->getAttribute('href');
	        if (0 !== strpos($href, 'http')) {
	            $path = '/' . ltrim($href, '/');
	            if (extension_loaded('http')) {
	                $href = http_build_url($url, array('path' => $path));
	            } else {
	                $parts = parse_url($url);
	                $href = $parts['scheme'] . '://';
	                if (isset($parts['user']) && isset($parts['pass'])) {
	                    $href .= $parts['user'] . ':' . $parts['pass'] . '@';
	                }
	                $href .= $parts['host'];
	                if (isset($parts['port'])) {
	                    $href .= ':' . $parts['port'];
	                }
	                $href .= dirname($parts['path'], 1).$path;
	            }
	        }
	        crawl_page($href, $depth - 1);
	    }
	    echo "URL:",$url,PHP_EOL,"CONTENT:",PHP_EOL,$dom->saveHTML(),PHP_EOL,PHP_EOL;
	}
}
