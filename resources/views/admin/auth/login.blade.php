<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>{{@__('auth.project_name')}} | Log in</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="{{ asset('public/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="{{ asset('public/assets/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="{{ asset('public/assets/css/AdminLTE.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/assets/custom.css') }}" rel="stylesheet" type="text/css" />
        <link rel="icon" type="text/css" href="{{ str_replace('index.php', '', url('demo_images/Physch_App_Logo.png')) }}" type="image/x-icon">

    </head>
    <body class="bg-black">

        <div class="form-box" id="login-box">
            @include("admin.admin_error")
            <div class="header">Sign In</div>
            <form action="{{ url('admin/login') }}" method="post">
                @csrf
                <div class="body bg-gray">
                    <div class="form-group">
                        <input type="text" name="email" class="form-control" placeholder="Email"/>
                        @if($errors->has("email"))
                            <span class="has-error">{{ $errors->first("email") }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Password"/>
                        @if($errors->has("password"))
                            <span class="has-error">{{ $errors->first("password") }}</span>
                        @endif
                    </div>          
                    <div class="form-group">
                        {{-- <input type="checkbox" name="remember_me"/> Remember me --}}
                    </div>
                </div>
                <div class="footer">                                                               
                    <button type="submit" class="btn bg-olive btn-block">Sign me in</button>  
                    
                    <p><a href="{{ url('admin/password/reset') }}">I forgot my password</a></p>
                    
                </div>
            </form>

        </div>


        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="{{ asset('public/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>        

    </body>
</html>