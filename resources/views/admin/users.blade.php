@extends('admin.layout.layout')

@section('content')
   <!-- Content Header (Page header) -->
   <section class="content-header">
       <h1>
           Users
       </h1>
       <ol class="breadcrumb">
           <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
           <li><a href="javascript:;">Users</a></li>
           <li class="active">View Users</li>
       </ol>
   </section>

   <!-- Main content -->
   <section class="content">
       @include("admin.admin_error")
       <div class="row">
           <div class="col-xs-12">
               
               <div class="box">
                   <div class="box-header">
                      <!--  <h3 class="box-title pull-left">Users</h3>   -->
                       <form method="GET" action="{{url('admin/users/')}}">
                         <div class="col-md-6 input-group pull-left ">
                            <input type="text" class="form-control" name="str" placeholder="Search Users by City, Name, Email, Mobile No" value="{{@$_GET['str']}}">
                             <span class="input-group-btn">
                               <button class="btn btn-default btn-flat" type="button" onclick="this.form.submit()">
                                <i class="fa fa-search"></i>
                               </button>
                            </span>
                          </div>  
                          <div class="col-md-2 pull-left" style="padding-right: 0">
                             <select class="form-control" name="searchby" onchange="this.form.submit()">
                              <option value="">--Search by Status--</option>
                              <option value="1" <?php if(@$_GET['searchby'] == '1') echo "selected" ?> >Active</option>
                              <option value="3" <?php if(@$_GET['searchby'] == '3') echo "selected" ?> >Inactive</option>
                            </select>
                          </div>
                          <div class="col-md-2 pull-left ">
                            <select class="form-control" name="sortby" onchange="this.form.submit()">
                              <option value="">--Sort by--</option>
                              <option value="created_at-asc" <?php if(@$_GET['sortby'] == 'created_at-asc') echo "selected" ?> >Date(ASC)</option>
                              <option value="created_at-desc" <?php if(@$_GET['sortby'] == 'created_at-desc') echo "selected" ?> >Date(DESC)</option>
                              <option value="name-asc" <?php if(@$_GET['sortby'] == 'name-asc') echo "selected" ?> >Name(A-Z)</option>
                              <option value="name-desc" <?php if(@$_GET['sortby'] == 'name-desc') echo "selected" ?> >Name(Z-A)</option>
                              <option value="email-asc" <?php if(@$_GET['sortby'] == 'email-asc') echo "selected" ?> >Email(A-Z)</option>
                              <option value="email-desc" <?php if(@$_GET['sortby'] == 'email-desc') echo "selected" ?> >Email(Z-A)</option>
                            </select>
                          </div>
                          <a href="{{url('admin/users/')}}" class="btn btn-flat btn-default pull-left">
                          Reset</a>
                        </form>                                
                   </div><!-- /.box-header -->
                   <div class="box-body table-responsive">
                       <table id="example1" class="table table-bordered table-striped">
                           <thead>
                               <tr>
                                   <th>S.No.</th>
                                   <th>Name</th>
                                   <th>Userimage</th>
                                   <th>Email</th>
                                   <th>Mobile</th>
                                   <th>Status</th>
                                   <th>Registered at</th>
                                   <th>Action</th>
                               </tr>
                           </thead>
                           <tbody>
                            <?php if(count($users) > 0){ ?>
                               @foreach ($users as $element)
                                   
                                   <tr>

                                       <td>{{ $loop->index+1 }}</td>
                                       <td>{{ $element->name }}</td>
                                       <td>
                                        <img src="@if(@$element->image){{ str_replace("index.php", "", url('storage/user_images/'.$element->image)) }} @else {{ str_replace("index.php","", url('public/assets/img/no-img.png')) }} @endif" style="width: 60px;">
                                        </td>
                                       <td>{{ $element->email }}</td>
                                       <td>{{ $element->mobile }}</td>
                                       <td >
                                        <span class="badge @if($element->status==1) bg-green @else bg-maroon @endif"> @if($element->status==1) Active @else Inactive @endif
                                        </span>
                                       </td>
                                        <td>{{ date('d M y, h:i a',strtotime($element->created_at)) }}</td>
                                       <td>@if($element->status==1)<a href="{{ url('admin/userdeactive/'.$element->id) }}" class="btn btn-flat bg-maroon btn-xs" onclick="return confirm('Are you sure, you want to Inactive {{ $element->name }}')">Inactive</a> @else <a href="{{ url('admin/useractive/'.$element->id) }}" class="btn btn-success btn-flat btn-xs" onclick="return confirm('Are you sure, you want to Active {{ $element->name }}')">Active</a> @endif</td>
                                       
                                   </tr>
                               
                               @endforeach
                               <?php 
                             }else{ ?>
                               <tr> 
                                <td class="text-center" colspan="8"> Record not found...!</td>
                              </tr>
                             <?php }
                             ?>
                           </tbody>
                           
                       </table>
                   </div><!-- /.box-body -->
               </div><!-- /.box -->
           </div>
       </div>

   </section><!-- /.content -->
@endsection

@section('myjsfile')
  
  <script src="{{ asset('assets/js/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script> 
  <script src="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script> 
  <script type="text/javascript">
      $(function() {
          $("#example1").dataTable();
          $('#example2').dataTable({
              "bPaginate": true,
              "bLengthChange": false,
              "bFilter": false,
              "bSort": true,
              "bInfo": true,
              "bAutoWidth": false
          });
      });
  </script>

@endsection