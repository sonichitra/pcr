@extends('admin.layout.layout')

@section('content')
 <section class="content-header">
     <h1>
         Contact Us
     </h1>
     <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Contact Us</li>
     </ol>
 </section>

 <!-- Main content -->
 <section class="content">
     @include("admin.admin_error")
     <div class="box">
         <div class="box-header">
             {{-- <h3 class="box-title">Data Table With Full Features</h3>                                     --}}
         </div><!-- /.box-header -->
         <div class="box-body table-responsive">
             <table id="example1" class="table table-bordered table-striped">
                 <thead>
                     <tr>
                         <th>S.No.</th>
                         <th>Name</th>
                         <th>Email</th>
                         <th>Mobile</th>
                         <th>Message</th>
                         <th>Message Reply</th>
                         <th>Status</th>
                         <th>Created At</th>
                         <th>Updated At</th>
                         <th>Action</th>
                     </tr>
                 </thead>
                 <tbody>
                     @foreach ($list as $element)
                     <p id="paragraph{{ $element->id }}" style="display: none;">{{ $element->message }}</p>
                     <p id="paragraph1{{ $element->id }}" style="display: none;">{{ $element->message_reply }}</p>
                       <tr>
                           <td>{{ $loop->index+1 }}</td>
                           <td>{{ $element->name }}</td>
                           <td>{{ $element->email }}</td>
                           <td>{{ $element->mobile }}</td>
                           <td onclick="messageView({{ $element->id }},1);"><a href="javascript:;">{{ mb_strimwidth($element->message, 0, 50, "...") }}</a></td>
                           <td @if($element->message_reply) onclick="messageView({{ $element->id }},2);" @endif><a href="javascript:;">{{ mb_strimwidth($element->message_reply, 0, 50, "...") }}</a></td>
                           @if($element->status==1)<td class="text-warning">Pending</td>@else<td class="text-success">Replied</td> @endif
                           <td>{{ $element->created_at }}</td>
                           <td>{{ $element->updated_at }}</td>
                           <td><a href="javascript:;" onclick="reply({{ $element->id }})">Reply</a></td>
                       </tr>
                     @endforeach
                 </tbody>
             </table>
         </div><!-- /.box-body -->
     </div><!-- /.box -->
 </section><!-- /.content -->
 <div id="myModalView" class="modal fade" role="dialog">
   <div class="modal-dialog">

     <!-- Modal content-->
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title">View Message</h4>
       </div>
       <div class="modal-body" id="paragraph_view">
         <p>Some text in the modal.</p>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       </div>
     </div>

   </div>
 </div>

 <div id="myModalReply" class="modal fade" role="dialog">
   <div class="modal-dialog">

     <!-- Modal content-->
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title">Reply User</h4>
       </div>
       <div class="modal-body">
         <form action="{{ url('admin/contactus') }}" method="POST">
           @csrf
           {{-- @method('patch') --}}
           <input type="hidden" name="id" id="contact_id" value="{{ old('id') }}">
           @csrf
           <div class="form-group">
             <label>Enter Your Reply Here</label>
             <textarea class="form-control" name="message_reply"></textarea>
           @if($errors->has("message_reply"))
             <span class="has-error">{{ $errors->first("message_reply") }}</span>
           @endif
           </div>
           <input type="submit" name="submit" value="Submit" class="btn btn-primary">
         </form>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       </div>
     </div>

   </div>
 </div>
@endsection
@section('myjsfile')
 <script type="text/javascript">
     $(function() {
         $("#example1").dataTable();
         $('#example2').dataTable({
             "bPaginate": true,
             "bLengthChange": false,
             "bFilter": false,
             "bSort": true,
             "bInfo": true,
             "bAutoWidth": false
         });
         @if (count($errors) > 0)
           $("#myModalReply").modal("show");
         @endif
     });

     function reply(id){
      $("#myModalReply").modal("show");
      $("#contact_id").val(id);
     }

     function messageView(id,type){
       if(type==1){
         var paragraph = $("#paragraph"+id).html();
       }else{
         var paragraph = $("#paragraph1"+id).html();
       }
       $("#myModalView").modal("show");
       $("#paragraph_view").html(paragraph);
     }

 </script>
@endsection



