@extends('admin.layout.layout')


@section('top_title', "Admin | Privacy Policy")

@section('content')
 <section class="content-header">
     <h1>
         Privacy Policy
     </h1>
     <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Privacy Policy</li>
     </ol>
 </section>
 <section class="content">
    <form method="POST" action="">

        {{ csrf_field() }}

          <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <textarea class="form-control summernote" name="detail">{{ @$terms->content }}</textarea>

            </div>
            @if(@$errors->has('detail'))
               <span class="has-error">{{ $errors->first("detail") }}</span>
            @endif

          </div>

          <div class="col-xs-12 col-sm-12 col-md-12 text-center">

            <button type="submit" class="btn btn-primary">Submit</button>

          </div>

    </form>
  </section>
@endsection

@section('myjsfile')
<script type="text/javascript">
     $('.summernote').summernote({
           height: 300,
      });
</script>
  
@endsection