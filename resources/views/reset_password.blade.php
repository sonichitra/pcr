<!DOCTYPE html>
<html class="bg-black">

    <head>
        <meta charset="UTF-8">
        <title>{{@__('auth.project_name')}} | Reset Password</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="{{ asset('public/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="{{ asset('public/assets/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="{{ asset('public/assets/css/AdminLTE.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/assets/custom.css') }}" rel="stylesheet" type="text/css" />
        <link rel="icon" type="text/css" href="{{ str_replace("index.php", "", url('demo_images/Physch_App_Logo.png')) }}" type="image/x-icon">

    </head>
    <body class="bg-black">

        <div class="form-box" id="login-box">
          @include("admin.admin_error")
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="header">Reset Password</div>
            {{-- <p>You can reset your password here.</p> --}}
            <!-- <form action="{{ url('/admin/password/reset') }}" method="post"> -->
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/password/reset') }}">
                {{ csrf_field() }}

                <input type="hidden" name="token" value="{{ $token }}">
                <div class="body bg-gray">
                    <div class="form-group">
                        <input type="text" name="email" class="form-control" placeholder="Email"/>
                        @if($errors->has("email"))
                            <span class="has-error">{{ $errors->first("email") }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <input type="password" name="password" id="password" class="form-control" placeholder="Pasword"/>
                        @if($errors->has("password"))
                            <span class="has-error">{{ $errors->first("password") }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password"/>
                        @if($errors->has("password_confirmation"))
                            <span class="has-error">{{ $errors->first("password_confirmation") }}</span>
                        @endif
                    </div>
                    
                </div>
                <div class="footer">                                                               
                    <button type="submit" class="btn bg-olive btn-block">Reset Password</button>  
                    <a href="{{ url('admin/login') }}">Back To Login</a>
                    
                    
                </div>
            </form>

        </div>


        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="{{ asset('assets/js/bootstrap.min.js') }}" type="text/javascript"></script>        

    </body>
</html>