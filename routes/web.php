<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/artisan-issue', function () {
  Artisan::call('config:clear');
    // Artisan::call('storage:link');
    // Artisan::call('route:list');
    // Artisan::call('make:mail', ['name' => 'InstructorVerifyMail' ]);
    // Artisan::call('make:model', ['name' => 'UserWater' ]);
    // Artisan::call('make:controller',['name'=> 'Admin/FaqController'], '--resource');
     Artisan::call('migrate');
     // Artisan::call('make:migration', ['name' => 'create_user_water_table' ]);
     // Artisan::call('make:migration', ['name' => 'alter_user_id_into_nutrition_table' ]);
    dd(Artisan::output());
});




Route::get('/home', 'HomeController@index')->name('home');



Route::group(['prefix' => 'admin'], function () {
  Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'AdminAuth\LoginController@login');
  Route::post('/logout', 'AdminAuth\LoginController@logout')->name('logout');

  Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'AdminAuth\RegisterController@register');

  Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');

  

  Route::group(['middleware' => ['auth:admin']], function ()
  {
      Route::get("/dashboard","Admin\AdminDashboardController@index");

      Route::resource("/users","Admin\AdminUserController");
      Route::get("/userdeactive/{id}","Admin\AdminUserController@userdeactive");
      Route::get("/useractive/{id}","Admin\AdminUserController@useractive");


      Route::resource("/profile","Admin\ProfileController");
      Route::post("/updateprofile","Admin\ProfileController@updateProfile");

      Route::get("/changepassword","Admin\ProfileController@changepassword");
      Route::post("/updatepassword","Admin\ProfileController@updatePassword");
  

      Route::get("/temscondition","Admin\AdminDashboardController@term_condition");
      Route::post("/temscondition","Admin\AdminDashboardController@term_condition_update");

      Route::get("/policy","Admin\AdminDashboardController@policy");
      Route::post("/policy","Admin\AdminDashboardController@policy_update");

      // Route::resource("/contactus","Admin\ContactUsController");

      // Route::get('/news', 'Admin\NewsController@index');
      // Route::get("/newsdeactive/{id}","Admin\NewsController@newsdeactive");
      // Route::get("/newsactive/{id}","Admin\NewsController@newsactive");
      // Route::get("/news/detail/{id}","Admin\NewsController@detail");

      ;
       
  });


});
