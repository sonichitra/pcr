<?php


use Illuminate\Http\Request;
// use Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// for reset password > https://medium.com/modulr/api-rest-with-laravel-5-6-passport-authentication-reset-password-part-4-50d27455dcca
//php artisan make:controller controller-name
Route::group(['prefix'=>'v1'], function(){

	Route::post('user/login', 'Api\UsersController@login');

	//Route::post('user/forgetpassword', 'Api\UsersController@forgetpassword');
	Route::post('user/forgetpassword', 'Api\PasswordResetController@create');
    Route::get('password/find/{token}', 'Api\PasswordResetController@find');
    Route::post('password/resetpassword', 'Api\PasswordResetController@reset');
	

	Route::group(['middleware' => 'auth:api'], function(){
		Route::post('user/details', 'Api\UsersController@details');
		Route::get('user/logout', 'Api\UsersController@logout');
		Route::post('user/profile', 'Api\UsersController@changeProfile');
		Route::post('user/contactus', 'Api\UsersController@contact_us');
		Route::get('privacy_policy', 'Api\UsersController@privacyPolicy');
		Route::get('term_condition', 'Api\UsersController@termCondition');
	});

});

